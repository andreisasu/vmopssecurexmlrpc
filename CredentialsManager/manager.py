#!/usr/bin/env python2
import sqlite3
from Crypto.Cipher import AES
#from Crypto import Random
import base64
import hashlib


class Credential():

    def __init__(self, master_password, dbname='credentials'):
        """
        :param master_password: password to unlock the database
        : you need to reinstantiate the Credential() object if you want to use another
        : master_password
        """
        self.dbname = dbname
        self.master_password = master_password
        self.iv = hashlib.sha256(self.master_password).hexdigest()
        self.PADDING = '{'
        self.pad = lambda p: p + (AES.block_size - len(p) % AES.block_size) * self.PADDING
        self.check_for_db(self.dbname)
        self.key = self.pad(master_password)

    def check_for_db(self, dbname):
        try:
            #print dbname
            conn = sqlite3.connect("credentials")
            cursor = conn.cursor()
            cursor.execute("""select * from store""")
            cursor.close()
            conn.commit()
            conn.close()
        except Exception, e:
            #let's create the table:
            cursor.execute("""CREATE TABLE STORE(machine_id integer PRIMARY KEY,
            name varchar,
            username varchar,
            password varchar) """)
            cursor.execute("""CREATE INDEX machine_id ON STORE (machine_id)""")
            cursor.close()
            conn.commit()
            conn.close()

    def store(self, machine_id=None, vm=None, username=None, password=None):
        """ the scope of this function is to store the credentials if they cannot be found
         credentials = {}
         credentials are stored in an sqlite database and queried each time this function needs to run for a test
         vm = string containing vm name
         username = string
         password = string
         """
        if vm and username and password:
            # encrypt password first:
            cipher = AES.new(self.key, AES.MODE_CFB, self.iv[-16:])
            encrypted_password = base64.b64encode(self.iv[-16:] + cipher.encrypt(password))

        try:
            conn = sqlite3.connect(self.dbname)
            cursor = conn.cursor()
            cursor.execute("""INSERT INTO STORE VALUES (?,?,?,?)""", [machine_id, vm, username, encrypted_password])
#            conn.commit()
        except Exception, e:
            print e
            return 1
        cursor.close()
        conn.commit()
        conn.close()

    def retrieve(self, id):
        """
        :param id: the primary key - unique id
        :param vm: the name of the host
        :return: decrypted password
        """
        conn = sqlite3.connect(self.dbname)
        cursor = conn.cursor()
        cursor.execute("""SELECT password from store where machine_id = ? """, (id, ))
        content = cursor.fetchall()
        if not content:
            print "nothing found"
            return None
        encrypted_password = str(content)
        #print "encrypted password %s" % encrypted_password
        # ugly hack to remove first & last 3 characters of the ret string
        encoded_password = encrypted_password[3:-3]
        #print "encoded password: %s" % encoded_password

        cipher = AES.new(self.key, AES.MODE_CFB, self.iv[-16:])
        #print "valoarea vectorului de initializare: %s" % self.iv[-16:]
        #print "valoarea self.master_password %s" % self.master_password

        decrypted_password = cipher.decrypt(base64.b64decode(encoded_password))
        #print "decripted password: %s " % decrypted_password[len(self.iv[-16:]):]
        cursor.close()
        conn.close()
        return decrypted_password[len(self.iv[-16:]):]

if __name__ == '__main__':
    for i in range(10):
        credential = Credential('!@#4QWErrEWQ4#@!')
        credential.store(i, "testvm" + str(i), "testusername", 'testpassword!@#$%^^&**!)@#^$')

    print "running decrypt test"
    for i in range(10):
        print credential.retrieve(i)
