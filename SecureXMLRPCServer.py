#!/usr/bin/env python2.7
"""
Part of this code is based on the following Activestate Recipes:

SecureXMLRPCServer.py - simple XML RPC server supporting SSL.
Based on this article: http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/81549
http://code.activestate.com/recipes/501148-xmlrpc-serverclient-which-does-cookie-handling-and/
http://code.activestate.com/recipes/442473-simple-http-server-supporting-ssl-secure-communica/
http://code.activestate.com/recipes/496786-simple-xml-rpc-server-over-https/

"""
import sys
# Configure below
LISTEN_HOST='127.0.0.1' # You should not use '' here, unless you have a real FQDN.
LISTEN_PORT=4433
KEYFILE='key.pem'    # Replace with your PEM formatted key file
CERTFILE='certificate.pem'  # Replace with your PEM formatted certificate file
VmwareObject = None
# Configure above

import SocketServer
import BaseHTTPServer
#import SimpleHTTPServer
import SimpleXMLRPCServer
import socket
from OpenSSL import SSL
import datetime

### needed for the client auth implementation
import uuid
import base64
import shelve
import Cookie
import traceback


## my own files:
import CredentialsManager
from Virtualization import VMware

class UserManagement:
    def __init__(self):
        self.d = shelve.open('credstore.shv')

        # register a list of valid machine names/email id's 
        validconfig = {'andrei.sasu42@gmail.com':'!@#4QWEr'}
        for k,v in validconfig.items():
            self.generateUuid(k,v)

    def generateUuid(self, email_id, user_password):
        """ return a uuid which uniquely identifies user_password and email id """

        uuidstr = None
        if user_password not in self.d:
            myNamespace = uuid.uuid3(uuid.NAMESPACE_URL, user_password)
            uuidstr = str(uuid.uuid3(myNamespace, email_id)) 

            self.d[user_password] = (user_password, uuidstr, email_id)
            self.d[uuidstr] = (user_password, uuidstr ,email_id)
        else:
            (user_password, uuidstr, email_id) = self.d[user_password]

        return uuidstr

    def checkMe(self, id):
        if id in self.d:
            return self.d[id]
        return (None,None,None)

    def __del__(self):
        self.d.close()

def authenticate(id):
    sk = UserManagement()
    return sk.checkMe(id)


class SecureXMLRPCServer(SocketServer.TCPServer, BaseHTTPServer.HTTPServer, SimpleXMLRPCServer.SimpleXMLRPCDispatcher):
    def __init__(self, server_address, HandlerClass, logRequests=True):
        """Secure XML-RPC server.

        It it very similar to SimpleXMLRPCServer but it uses HTTPS for transporting XML data.
        """
        self.logRequests = logRequests

        SimpleXMLRPCServer.SimpleXMLRPCDispatcher.__init__(self, allow_none=True)
        SocketServer.BaseServer.__init__(self, server_address, HandlerClass)
        ctx = SSL.Context(SSL.SSLv23_METHOD)
        ctx.use_privatekey_file (KEYFILE)
        ctx.use_certificate_file(CERTFILE)

        self.socket = SSL.Connection(ctx, socket.socket(self.address_family,
                                                        self.socket_type))
        self.server_bind()
        self.server_activate()

    def shutdown_request(self,request):
        """Called to shutdown and close an individual request."""
        """
        needed to work around: https://bugs.launchpad.net/pyopenssl/+bug/900792
        """
        try:
            request.shutdown()
        except Exception as e:
            print("Cannot close connection: %s" % e)
            traceback.print_exc()#some platforms may raise ENOTCONN here
        finally:
            self.close_request(request)

    def close_request(self, request):
        """Called to clean up an individual request."""
        request.close()

        
class SecureXMLRpcRequestHandler(SimpleXMLRPCServer.SimpleXMLRPCRequestHandler):
    """Secure XML-RPC request handler class.

    It it very similar to SimpleXMLRPCRequestHandler but it uses HTTPS for transporting XML data.
    """
    def setup(self):
        self.connection = self.request
        self.rfile = socket._fileobject(self.request, "rb", self.rbufsize)
        self.wfile = socket._fileobject(self.request, "wb", self.wbufsize)


    def setCookie(self, key=None ,value=None):
        if key :
            c1 = Cookie.SimpleCookie()
            c1[key] = value
            cinfo = self.getDefaultCinfo() 
            for attr,val in cinfo.items():
                c1[key][attr] = val
                            
            if c1 not in self.cookies:
                self.cookies.append(c1)

    def getDefaultCinfo(self):
        cinfo = {}

        cinfo['expires'] = 30*24*60*60
        cinfo['path'] = '/RPC2/'
        cinfo['comment'] = 'comment!'
        cinfo['domain'] = '.localhost.local'
        cinfo['max-age'] = 30*24*60*60
        cinfo['secure'] = ''
        cinfo['version']= 1
        
        return cinfo    
        
    
    def do_POST(self):
        """Handles the HTTPS POST request.
        It was copied out from SimpleXMLRPCServer.py and modified to shutdown the socket cleanly.
        """

        try:
            # get arguments

            # hack to support client auth
            max_chunk_size=10*1024*1024
            size_remaining = int(self.headers["content-length"])
            L = []
            while size_remaining:
                chunk_size = min(size_remaining, max_chunk_size)
                L.append(self.rfile.read(chunk_size))
                size_remaining -= len(L[-1])
            data = ''.join(L)
            ####

            #data = self.rfile.read(int(self.headers["content-length"]))

            # In previous versions of SimpleXMLRPCServer, _dispatch
            # could be overridden in this class, instead of in
            # SimpleXMLRPCDispatcher. To maintain backwards compatibility,
            # check to see if a subclass implements _dispatch and dispatch
            # using that method if present.
            response = self.server._marshaled_dispatch(
                    data, getattr(self, '_dispatch', None)
                )
        except Exception as e: # This should only happen if the module is buggy
            # internal error, report as HTTP server error
            print("exception occured: %s" % e)
            self.send_response(500)
            self.end_headers()
        else:
            # got a valid XML RPC response
            self.send_response(200)
            self.send_header("Content-type", "text/xml")
            self.send_header("Content-length", str(len(response)))
            self.end_headers()
            self.wfile.write(response)

            # hack start  -> sends cookies here
            if self.cookies:
                for cookie in self.cookies:
                    self.send_header('Set-Cookie', cookie.output(header=''))
            # hack end
            self.end_headers()
            self.wfile.write(response)
            # shut down the connection
            self.wfile.flush()

            #self.connection.shutdown()
            #self.connection.close() # Modified here!

    def authenticate_client(self):
        validuser = False

        if self.headers.has_key('Authorization'):
            # handle Basic authentication
            (enctype, encstr) =  self.headers.get('Authorization').split()
            (emailid, user_password) = base64.standard_b64decode(encstr).split(':')

            (auth_password, auth_uuidstr, auth_email) = authenticate(user_password)

            if (user_password == auth_password) and (emailid == auth_email):
                print "Authenticated"
                # set authentication cookies on client machines
                validuser = True
                if auth_uuidstr:
                    self.setCookie('UUID',auth_uuidstr)
                    
        elif self.headers.has_key('UUID'):
            # handle cookie based authentication
            id =  self.headers.get('UUID')
            (auth_password, auth_uuidstr, auth_email) = authenticate(id)

            if auth_uuidstr:
                print "Authenticated"
                validuser = True
        else:
            print 'Authentication failed'

        return validuser

    def _dispatch(self, method, params):
        self.cookies = []
        
        validuser = self.authenticate_client()
        exported_methods = ['connect','print_vm_names',
                                'put_files','run_command',
                                'clone_linked','destroyvm',
                                'return_inventory', 'migrate_vm']
        # handle request
        # additional checks, for security...
        if validuser:
            if method in exported_methods:
                global VmwareObject

                def connect():
                    global VmwareObject
                    retval = VMware.VmwareOperations().connect(*params)
                    VmwareObject = retval[0]
                    return retval[1] # this is a string for the client ( defined in ReturnCodes() class)

                def print_vm_names(VmwareObject=VmwareObject):
                    return VMware.VmwareOperations(VmwareObject).print_vm_names(params)

                def put_files(VmwareObject=VmwareObject):
                    return VMware.VmwareOperations(VmwareObject).put_files(*params)

                def run_command(VmwareObject=VmwareObject):
                    return VMware.VmwareOperations(VmwareObject).run_command(*params)

                def clone_linked(VmwareObject=VmwareObject):
                    return VMware.VmwareOperations(VmwareObject).clone_linked(*params)

                def destroyvm(VmwareObject=VmwareObject):
                    return VMware.VmwareOperations(VmwareObject).destroyvm(*params)

                def return_inventory(VmwareObject=VmwareObject):
                    return VMware.VmwareOperations(VmwareObject).return_inventory(params)

                def migrate_vm(VmwareObject=VmwareObject):
                    return VMware.VmwareOperations(VmwareObject).migrate_vm(*params)

                return {"connect": connect, "print_vm_names": print_vm_names, "put_files": put_files,
                 "run_command": run_command, "clone_linked": clone_linked, "destroyvm": destroyvm,
                 "return_inventory": return_inventory, "migrate_vm": migrate_vm}[method]()
            else:
                return 'Invalid Method [%s]' % method
        else:
            return 'Please register!'

if __name__ == '__main__':

    import threading
    class ServerThread(threading.Thread):
        def __init__(self):
            threading.Thread.__init__(self)
            self.HandlerClass=SecureXMLRpcRequestHandler
            self.ServerClass=SecureXMLRPCServer
            self.server_address = (LISTEN_HOST, LISTEN_PORT) # (address, port)
            self.localServer = self.ServerClass(self.server_address, self.HandlerClass)
            self.localServer.register_instance(VMware.VmwareOperations())

        def run(self):
            self.sa = self.localServer.socket.getsockname()
            print "Serving HTTPS on", self.sa[0], "port", self.sa[1]
            self.localServer.serve_forever()
    server = ServerThread()
    server.start()
