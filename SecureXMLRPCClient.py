#!/usr/bin/env python2.7
### client.py 
import os
import base64
import xmlrpclib
import urllib2
import cookielib
import exceptions
import time
import argparse

class CookieAuthXMLRPCTransport(xmlrpclib.SafeTransport):
    """ xmlrpclib.Transport that sends basic HTTP Authentication"""

    user_agent = '*py*'
    credentials = ()
    cookiefile = 'cookies.lwp'
        
    def send_basic_auth(self, connection):
        """Include HTTP Basic Authentication data in a header"""
        
        auth = base64.encodestring("%s:%s" % self.credentials).strip()
        auth = 'Basic %s' %(auth,)
        connection.putheader('Authorization',auth)

    def send_cookie_auth(self, connection):
        """Include Cookie Authentication data in a header"""
        
        cj = cookielib.LWPCookieJar()
        cj.load(self.cookiefile)

        for cookie in cj:
            if cookie.name == 'UUID':
                uuidstr = cookie.value
            connection.putheader(cookie.name, cookie.value)

    ## override the send_host hook to also send authentication info
    def send_host(self, connection, host):
        xmlrpclib.SafeTransport.send_host(self, connection, host)
        if os.path.exists(self.cookiefile):
            self.send_cookie_auth(connection)
        elif self.credentials != ():
            self.send_basic_auth(connection)
                    
    def request(self, host, handler, request_body, verbose=0):
        # dummy request class for extracting cookies 
        class CookieRequest(urllib2.Request):
            pass
            
        # dummy response class for extracting cookies 
        class CookieResponse:
            def __init__(self, headers):
                self.headers = headers
            def info(self):
                return self.headers 

        crequest = CookieRequest('https://'+host+'/')
            
        # issue XML-RPC request
        h = self.make_connection(host)
        if verbose:
            h.set_debuglevel(1)

        self.send_request(h, handler, request_body)
        self.send_host(h, host)
        self.send_user_agent(h)
        
        # creating a cookie jar for my cookies
        cj = cookielib.LWPCookieJar()
                        
        self.send_content(h, request_body)
        response = h.getresponse()
        errcode, errmsg, headers = response.status, response.reason, response.msg

        cresponse = CookieResponse(headers)
        cj.extract_cookies(cresponse, crequest)

        if len(cj) >0 and self.cookiefile != None:
            cj.save(self.cookiefile)
                
        if errcode != 200:
            raise exceptions.BaseException(
                host + handler,
                errcode, errmsg,
                headers
                )

        self.verbose = verbose

        try:
            sock = h._conn.sock
        except AttributeError:
            sock = None
        raspuns = self.parse_response(response)
        return raspuns

def getXmlrpcClient(server_uri, auth = ()):
    """ this will return an xmlrpc client which supports
    basic authentication/authentication through cookies 
    """

    trans = CookieAuthXMLRPCTransport()
    if auth != ():
        trans.credentials = auth
    client = xmlrpclib.Server(server_uri, transport=trans, verbose=False, allow_none=True)

    return client

if __name__ == "__main__":
    email = 'andrei.sasu42@gmail.com'
    password = '!@#4QWEr'
    
    client = getXmlrpcClient('https://localhost:4433', (email, password))
    inputstr1 = "hello"
    inputstr2 = "world"

    try:
        from Colors import bcolors
        message = bcolors.MessageColors()

        retstr = client.welcome()
        print retstr
        retstr = client.hello_world(inputstr1,inputstr2)
        print retstr
        print client.hey('hey')
        #
        list = ['vc2.np-unix.dsd.ro', 'root', 'vmware']

        ### the name of the virtual machine, as it appears in the vsphere client:
        appliance = 'ORIGINAL_unpack_reboot_Windows_x32_esx5-8.np-unix.dsd.ro'
        print 'attempting connect(): %s' % client.connect(*list)
        print("""
        Found hosts:
        %s
        """ % client.return_inventory("hosts"))
        print("""
        Found datacenters:
        %s
        """ % client.return_inventory("datacenters"))
        print("""
        Found virtual machines:
        %s
        """ % client.return_inventory("registered_vms"))
        print("""
        Found resource pools:
        %s
        """ % client.return_inventory("resource_pools"))

        print("""
        Found clusters:
        %s
        """ % client.return_inventory("clusters"))

        #
        print 'attempting put_files(): '
        print '%s' % client.put_files("/tmp/test.txt", "/tmp/test.txt.2", appliance, "root", "!@#4QWEr", "/tmp") 

        print 'attempting run_command(): '
        print '%s' % client.run_command(appliance,"/sbin/ifconfig","root","!@#4QWEr", ["eth0","10.10.50.247","netmask","255.255.0.0","up"]) 
        client.run_command(appliance,"/sbin/route","root","!@#4QWEr", ["add", "default", "gw", "10.10.0.1"])

        print 'attempting clone_linked(): '
        all_clones = []
        all_clones += client.clone_linked(appliance, "infectedVM", "starting", 5, True)
        print '%s' % all_clones
        print 'attempting destroyvm(''%s' % all_clones+') '
        client.destroyvm(*all_clones)
    except Exception, e:
        print e
