import sys
from pysphere import VIServer, MORTypes
import dbm
import getpass
import datetime
import thread
import multiprocessing
import time
import traceback
import Queue

from RetCodes.codes import *


class VmwareOperations():

    def __init__(self, s=VIServer()):
    ################################# global variables / dictionaries / lists##############################
        self.toate_clonele = []  # a list containing all the clones we have cloned
        self.retrieved_vms = {}  # a dictionary containing all VMs
        self.s = s#VIServer()   # the VCENTER server object...very important
        # used in displaying the VMs and generating trees ( by folder , by dc, by resource pool .. etc )
        self.by_what = {}
        self.thread_count = 8  # how many clones simultaneously?
        self.credentials = {} # used as a temporary cache so we don't query the db each time
        import string
        self.python_string = string
        self.retval = ReturnCodes()

        try:
            if self.s.is_connected:
                self.sync = self.Synchronize(self.s)
        except AttributeError:
            print 'No client connected yet, so not syncing any inventory. '
            self.sync = self.Synchronize()

    class Synchronize(VIServer):

        def __init__(self, VIServerobject=None):

            """ all the below are dictionaries
             and should be shown like this:
             sync = Synchronize(VIServer_instance)
             sync.hosts.items()
             sync.datastores.items() """

            if not VIServerobject:
                return None # we are not connected yet
            else:
                self.hosts = VIServerobject.get_hosts()
                self.datacenters = VIServerobject.get_datacenters()
                self.datastores = VIServerobject.get_datastores()
                self.registered_vms = VIServerobject.get_registered_vms()
                self.resource_pools = VIServerobject.get_resource_pools()
                self.clusters = VIServerobject.get_clusters()
                self.vm_names = []

        def resync(self, Synchronizeobject):
            f = super(self, Synchronizeobject).__init__()
            if (f.hosts() == Synchronizeobject.hosts)and(f.datacenters == Synchronizeobject.datacenters) and \
                    (f.datastores == Synchronizeobject.datastores) and \
                    (f.registered_vms == Synchronizeobject.registered_vms):
                return "nothing changed"
            else:
                return f

    def welcome(self):
        return("""Welcome to VMware Tool 0.1 \n,
        Type connect() to initiate a vcenter session \n
        clone() => hard-clones a VM \n
        put_files() => uploads files to guest \n
        print_vm_names() => All the VMs with their corresponding .vmx file \n
        run_command() => distributed shell, runs commands on many VMs \n
        destroyvm() => Equivalent to Delete from disk in Vsphere Client \n
        clone_linked() => Same as clone() only much faster. \n
                Requires a snapshot of the source VM to be present.\n""")

    def return_inventory(self,what):
        """
        should be called from the client like this:
        server.return_inventory("hosts")
        server.return_inventory("registered_vms")
        """
        self.what = what
        if self.what[0] == 'hosts': return str(self.sync.hosts.items()) #dict
        if self.what[0] == 'datacenters': return str(self.sync.datacenters.items()) #dict
        if self.what[0] == 'registered_vms': return str(self.sync.registered_vms) #list
        if self.what[0] == 'resource_pools': return str(self.sync.resource_pools.items()) #dict
        if self.what[0] == 'clusters': return str(self.sync.clusters.items()) # ?
        else:
            return None

    def connect(self, vcenter_addr=None, vcenter_username=None, vcenter_password=None):
        """Besides connecting to the VCENTER, this will put an object
        named sync on the global scope which can be reused
        to refresh the VCENTER inventory whenever needed
        called like this:

        server.connect(*list_of_parameters)
        """
        try:
            self.s.connect(vcenter_addr,vcenter_username, vcenter_password, trace_file="debug.txt")
            self.sync = self.Synchronize(self.s)
            return (self.s, self.retval.LIVE_MESSAGE['OPERATION_SUCCESSFUL'][0])
        except Exception, e:
            return self.retval.LIVE_MESSAGE['OPERATION_FAILED'][0]

    def __retrieve_vm_names(self):
        """
        returns a dictionary containing 
        { vmname : vmx_file }
        """
        result = self.s._retrieve_properties_traversal(property_names=["name", "config.files.vmPathName"],
                                                  obj_type="VirtualMachine")
        vm_names = {}
        for r in result:
            name = None
            path = None
            for prop in r.PropSet:
                if prop.Name == "name":
                    name = prop.Val
                elif prop.Name == "config.files.vmPathName":
                    path = prop.Val
            vm_names[path] = name
        return vm_names

    def __store_vm_names(self,fisier=None):
        global retrieved_vms
        retrieved_vms = self.__retrieve_vm_names()
        if fisier:
            db=dbm.open(fisier, 'c')
            for fisier_vmx, nume_masina in retrieved_vms.iteritems():
                db[fisier_vmx] = nume_masina
            db.close()
            return fisier
        return retrieved_vms

    def print_vm_names(self, by_what):
        """returns a list of VMs, grouped by certain elements.
        by_what must be one of the following: "datacenters", "datastores", "resourcepools", "clusters" """
        self.retrieved_vms = self.__retrieve_vm_names()
        self.by_what = by_what
        def datacenters():
            toate = []
            datacenters = self.sync.datacenters
            mors=self.s._get_managed_objects("Folder")
            for dc,nume_dc in datacenters.items():
                print("\n", "Datacenter %s" % nume_dc, " contains: \n")

                if self.retrieved_vms:
                    for vm, fisier_vmx in self.retrieved_vms.items():
                        print("%s" % vm)
                for vm in self.s.get_registered_vms(datacenter=dc):
                    print("%s " % vm)
                    toate.append(vm)
            return(toate)

        def datastores():
            datastores = self.sync.datastores
            for dc, nume_ds in datastores.items():
                print("\n", "Datastore %s" % nume_ds, " contains: \n")
                if self.retrieved_vms:
                    for fisier_vmx, vm in self.retrieved_vms.items():
                        search = fisier_vmx
                        try:
                            if search[search.find("[")+1:search.find("]")] == nume_ds:
                                print("%s " % vm)
                            else:
                                continue
                        except:
                            continue

        def resourcepools():
            return self.s.get_resource_pools()

        def clusters():
            print"clusters placeholder"

        def folders():
            print"folders placeholder"

        try:
            {"datacenters": datacenters, "datastores": datastores, "resourcepools": resourcepools, "clusters": clusters,
             "folders": folders, }[self.by_what[0]]()
        except Exception, e:
            print ("Dont know how to search by %s: %s " % (by_what, e) )

    def __search_vm_by_name(self, masina=None):
        """ return the first vmx file of the first vm found with matching name
        """
        self.retrieved_vms = self.__retrieve_vm_names()
        try:
            retval = [key for (key, value) in self.retrieved_vms.items() if value == masina][0]
            return retval
        except IndexError:
            return("Machine not found")

    def clone_linked(self, sursa=None, clone_name=None, snap=None, how_many=None, power_on=False):
        """  returns a list containing all the clones which have been created   """
        if sursa:
            sursa = self.__search_vm_by_name(sursa)
        for fisier_vmx, nume_masina in self.retrieved_vms.items():
            if nume_masina == sursa:
                sursa = self.retrieved_vms[sursa]
                print sursa
            if fisier_vmx == sursa:
                print("Attempting to clone %s " % sursa + " %s " % how_many + " times...")
                try:
                    instanta = self.s.get_vm_by_path(sursa)
                    def clone_engine(destinatie):
                        try:
                            #global message
                            print "-> %s" % datetime.datetime.now(), "Started worker thread "
                            instanta.clone(destinatie, sync_run=False, folder=None, resourcepool=None,
                                           datastore = None, host = None, power_on=True,
                                           template = False, snapshot=str(snap), linked=True)
                            print("-> %s " % datetime.datetime.now(), "%s" % destinatie +
                                                                         "Created! [OK]")
                        except Exception, e:
                            return(e)
                    jobs = []
                    for d in [x for x in range(how_many)]:
                        try:
                            destinatie = clone_name + "[" + str(d) + "]"
                            self.toate_clonele.append(destinatie)

                            p = multiprocessing.Process(target=clone_engine, args=(destinatie,))
                            jobs.append(p)

                        except Exception, e:
                            return(e)
                    [job.start() for job in jobs ]
                except Exception as e:
                    traceback.print_exc()

        print('-> %s' % datetime.datetime.now(), ' Cloned these vms: %s' % self.toate_clonele)

        # cleaning up
        while True:
            for job in jobs:
                if job.exitcode is not None:
                    job.terminate()
                    jobs.remove(job)
                else:
                    continue
            if len(jobs) == 0:
                break

        return self.toate_clonele

    def destroyvm(self, *nume_sau_lista):
        """powers off and removes from disk"""
        if nume_sau_lista:
            destroyed=[]
            for vm in nume_sau_lista:
                print("-> Searching for: %s " % vm)
                masina = self.s.get_vm_by_path(self.__search_vm_by_name(vm))
                if not masina.is_powered_off():
                    try:
                        print("Attempting to poweroff machine %s" % vm)
                        masina.power_off()
                    except Exception, e:
                        print("Something went wrong while powering off %s" % e)
                        return self.retval.LIVE_MESSAGE['GENERIC_FAIL']
                try:
                    masina.destroy(sync_run=False)
                    destroyed.append(vm)
                    continue
                except Exception, e:
                    print("Something went wrong %s", e)
                    return self.retval.LIVE_MESSAGE['GENERIC_FAIL']

            if len(destroyed) == len(nume_sau_lista):
                return self.retval.LIVE_MESSAGE['OPERATION_SUCCESSFUL'][4]
            else:
                return self.retval.LIVE_MESSAGE['GENERIC_FAIL']
        else:
            pass

    def migrate_vm(self, host, vm):
        vm = self.__search_vm_by_name(vm)
        vm = self.s.get_vm_by_path(vm)
        try:
            vm.migrate(host)
        except Exception as e:
            print(e)

    def put_files(self, source, destination, masina, username, parola, director=None):
        """
        director = this is only necessary if you want to display the size of the files you copied
        source = full path is required
        destination = full path is required
        masina = a string containing the VM name
        """
        masina = self.s.get_vm_by_path(self.__search_vm_by_name(masina))
        try:
            masina.login_in_guest(username, parola)
        except Exception, e:
            return self.retval.LIVE_MESSAGE['OPERATION_FAILED'][2]

        try:
            masina.send_file(source, destination, overwrite=True)
            for f in masina.list_files(director):
                print ("'%s' is a %s of %s bytes." % (f['path'], f['type'], f['size']))

        except Exception, e:
            print "something went wrong sending the file: %s " % e
            return self.retval.LIVE_MESSAGE['GENERIC_FAIL']

        return self.retval.LIVE_MESSAGE['OPERATION_SUCCESSFUL'][1]

    def run_command(self,
                    masina=None, command=None,
                    username=None, parola=None,
                    args_list=None, env=None, cwd=None ):

        """ 
        args_list=[] <- must be a list type like: 
        args_list=["-f","/"] if the command is rm :)
        """
        try:
            masina = self.s.get_vm_by_path(self.__search_vm_by_name(masina))
        except Exception as e:
            print(e)

        def prefetch_vm(masina):  # takes a VM object type as argument
            rezultat = {}
            if masina:
                status = masina.get_status()
                tools_status = masina.get_tools_status()
                lista = [status, tools_status]
            for stare in lista:
                rezultat['status'] = lista[0]
                rezultat['tools_status'] = lista[1]
            return rezultat

        fetch = prefetch_vm(masina)
        if (fetch['status'] == 'POWERED ON') and (fetch['tools_status'] == 'RUNNING' or 'RUNNING_OLD'):
            try:
                masina.login_in_guest(username, parola)
                pid = masina.start_process(command, args_list)
                if pid:
                    print("Command successful: %s " % str(command), "pid: %s status->[OK]" % pid)
                    return self.retval.LIVE_MESSAGE['OPERATION_SUCCESSFUL'][3]
                else:
                    print("Command %s didn't run status->[FAIL]" % (str(command), str(args_list)))
                    return self.retval.LIVE_MESSAGE['GENERIC_FAIL']
            except Exception, e:
                print("Something went wrong: %s " % (e, args_list))
                return self.retval.LIVE_MESSAGE['GENERIC_FAIL']

        if fetch['status'] != 'POWERED_ON' and fetch['tools_status'] != 'RUNNING':
            print("Is the VM powered on? status->[FAIL] %s" % fetch['status'])
            print("Are Vmtools installed? status->[FAIL] %s" % fetch['tools_status'])
            return self.retval.LIVE_MESSAGE['OPERATION_FAILED'][3]

    def __get_misc(self,vm=None):
        if not vm:
            vms = self.s.get_registered_vms()
        else:
            vms = [vm]
        for masina in vms:
            vm = self.s.get_vm_by_path(masina)
            for proprietate, valoare in vm.get_properties().iteritems():
                print(proprietate, valoare)
            pass

    def _get_all(self):
        # MOR = Managed Object Reference
        mors = self.s.get_datacenters().keys() + self.s.get_clusters().keys() + self.s.get_hosts().keys() + \
               self.s._get_managed_objects(MORTypes.VirtualMachine).keys()
        props = {MORTypes.VirtualMachine: ["name", "config.files.vmPathName"],
                 MORTypes.ClusterComputeResource: ["name"],
                 MORTypes.HostSystem: ["name", "runtime.powerState"],
                 MORTypes.Datacenter: ["name", "parent"],}
        result = self.s._get_object_properties_bulk(mors, props)
        for item in result:
            print "mor:", item.Obj
            for prop in item.PropSet:
                print " ", prop.Name, "=>", prop.Val
