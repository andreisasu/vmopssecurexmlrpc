class ReturnCodes():

    """ 
    this will replace all return codes in the future.
    return codes are organized as a list of tuples 
    the tuple contains the unique error code + a string with
    a meaningful message.

    For example: 
    retval = ReturnCodes()
    retval.LIVE_MESSAGE.items()[0][1][0] shows:
    (010, "Connnected!")
    """
    
    def __init__(self):
        self.OPERATION_SUCCESSFUL = "OPERATION_SUCCESSFUL"
        self.OPERATION_FAILED = "OPERATION_FAILED"
        self.VM_NOT_FOUND = "VM_NOT_FOUND"
        self.WRONG_CREDENTIALS = "WRONG_CREDENTIALS"
        self.GENERIC_FAIL = "GENERIC_FAIL"

        self.LIVE_MESSAGE = {
        self.OPERATION_SUCCESSFUL : [(110, "Connected!"),
                                     (111, "Upload Successful!"),
                                     (112, "Login Ok!"),
                                     (113, "Command sent!"),
                                     (114, "Operation succcessful!")],
        self.OPERATION_FAILED : [(210, "Connect failed!"),
                                 (211, "Upload failed!"),
                                 (212, "Login failed!"),
                                 (213, "VM is not ready!")],
        self.VM_NOT_FOUND : [(310, "VM not found!")],
        self.WRONG_CREDENTIALS : [(410, "Wrong username or password!")],
        self.GENERIC_FAIL : [(510, "Something went wrong!")],
        }

if __name__ == "__main__":
    retval = ReturnCodes()
    print retval.LIVE_MESSAGE['OPERATION_SUCCESSFUL'][0]
    print retval.LIVE_MESSAGE['OPERATION_FAILED'][2]
    print retval.LIVE_MESSAGE['VM_NOT_FOUND']
    print retval.LIVE_MESSAGE['WRONG_CREDENTIALS']